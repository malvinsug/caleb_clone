import pandas as pd 
import numpy as np
from ctgan import *
from ctgan import load_demo
from ctgan import CTGANSynthesizer
import random
import time
import traceback
import os,sys

# copy pasta
def gan_train(data, discrete_columns, model = CTGANSynthesizer(), epochs=1):
    model.fit(data, discrete_columns,  epochs=epochs)
    return model

def gan_load_and_train_model(data, path_to_model_gan, discrete_columns = []):

    model = CTGANSynthesizer()
    model.fit(data, discrete_columns, epochs=0, load_path=gan_model_path)
    #model.fit(data, discrete_columns, epochs=1, log_frequency = True)
    return model

def gan_sample(model, number_of_samples, class_focus='', value_focus=''):
    if class_focus != "" and value_focus != "":
        samples = model.sample(number_of_samples)
    else:
        samples = model.sample(number_of_samples)
    print(samples.head(10))
    return samples

def gan_save(model, path_to_model_gan):
    model.save(path_to_model_gan)
    print("Model saved")

def str_time_prop(start, end, format, prop):
    """Get a time at a proportion of a range of two formatted times.

    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """

    stime = time.mktime(time.strptime(start, format))
    etime = time.mktime(time.strptime(end, format))

    ptime = stime + prop * (etime - stime)

    return time.strftime(format, time.localtime(ptime))

def random_date(start, end, prop):
    return str_time_prop(start, end, '%Y-%m-%d', prop)


# main

#create_dir
gan_model_dir_path = "saved_model/gan_db_compile"
gan_model_path = gan_model_dir_path + "/gan_model"
try:
    os.mkdir(gan_model_dir_path)
except:
    pass


samples_size = 50

data = load_demo()
discrete_columns = [
    'workclass',
    'education',
    'marital-status',
    'occupation',
    'relationship',
    'race',
    'sex',
    'native-country',
    'income'
]

model = gan_train(data, discrete_columns=discrete_columns, model = CTGANSynthesizer(), epochs=1)
samples = gan_sample(model, number_of_samples=samples_size)

try:
    gan_save(model,gan_model_path)
except: 
    traceback.print_exc()